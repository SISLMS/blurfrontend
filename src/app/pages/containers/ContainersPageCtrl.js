

(function () {
  'use strict';

  angular.module('BlurAdmin.pages.containers')
      .controller('ContainersPageCtrl', ContainersPageCtrl);

  /** @ngInject */
  function ContainersPageCtrl($scope, $filter, $http, $stateParams, editableOptions, editableThemes, $mdDialog) {

	  var REST_SERVICE_URI = 'http://localhost:8081/BackEnd/api/';
	  var loggedInUserId = 1;

	    var self = this;
	    self.container={id:null, name:'', parentContainer:null, containerType:'', startDate:'', address:'', suspended:''};
      self.systemInstance={id:null, name:'', parentContainer:null, containerType:null, startDate:'', address:'', suspended:''};
      self.course={id:null, name:'', parentContainer:null, containerType:null, startDate:'', address:'', suspended:''};
	    self.containers=[];
      self.systemInstancesList=[];
      self.systemInstanceChildsList=[];
      self.coursesList=[];
	    self.availableContainerTypes=[];
	    self.containerTypesFiltered=[];
	    self.parentContainers=[];
      self.containersHierarchyForUser=[];
      self.containerTypesRelatedToSpecificContainer=[];
      self.currentContainer = {id:2, parentContainer:{id:1}};
      $scope.allGoals = [];
      self.test = [];
      self.goal = {};
      self.parentContainerGoalsList = [];
      // self.jstreeData0 = [
      //   { id : 90, parent : "#", text : "Simple root node" },
      //   { id : 91, parent : "#", text : "Root node 2" },
      //   { id : 93, parent : 91, text : "Child 1" },
      //   { id : 94, parent : 91, text : "Child 2" },
      //   { id : 95, parent : 93, text : "Child 3" },
      //   { id : 96, parent : 93, text : "Child 4" },
      //   { id : 97, parent : 94, text : "Child 5" },
      //   { id : 98, parent : 94, text : "Child 6" },
      //   ];
      self.jstreeData = [];
        self.node = {};



	    self.fetchAllContainers = function(){
	    	$http.get( REST_SERVICE_URI +'coreController/allcontainers/').then(
						       function(d) {
							        self.containers = d.data;
						       },
	      					function(errResponse){
	      						console.error('error while fetching all containers from controller');
	      					}
				       );
	    };

	    self.initContainerForm = function(){

        if ($stateParams.updateContainer != undefined) {
          self.updateSystemInstancesTypesFiltered($stateParams.updateContainer.parentContainer);
          self.container = $stateParams.updateContainer;
         }

         $http.get( REST_SERVICE_URI +'coreController/getContainerById/'+self.currentContainer.parentContainer.id).then(
                    function(resp1) {
                       self.containersHierarchyForUser.push(resp1.data);
                       $http.get( REST_SERVICE_URI +'coreController/allChildsSystemInstances/'+self.currentContainer.parentContainer.id).then(
                                  function(resp2) {
                                    for (var i = 0; i < resp2.data.length; i++) {
                                      self.containersHierarchyForUser.push(resp2.data[i]);
                                    }
                                  },
                                 function(errResponse){
                                   console.error('Error while getting containers Types RelatedToSpecific Container For User from controller');
                                 }
                              );
                    },
                   function(errResponse){
                     console.error('error while fetching system instance by id from controller');
                   }
                );



        //  var startFromSpecificContainer = 1;
        //  var firstLevelOnly = true;
	    	// $http.get( REST_SERVICE_URI +'coreController/containersHierarchyToBeSelectedFromUser/'+loggedInUserId+'/'+startFromSpecificContainer+'/'+firstLevelOnly).then(
				// 		       function(d) {
				// 		    	   self.containersHierarchyForUser = d.data;
				// 		       },
	      // 					function(errResponse){
	      // 						console.error('Error while getting containers Hierarchy For User from controller');
	      // 					}
				//        );
	    };


	    self.updateContainerTypesFiltered = function(){

      var containerId = self.container.parentContainer.id;
      $http.get( REST_SERVICE_URI +'coreController/getContainerTypesRelatedToSpecificContainer/'+containerId).then(
                 function(d) {
                   self.containerTypesRelatedToSpecificContainer = d.data;
                 },
                function(errResponse){
                  console.error('Error while getting containers Types RelatedToSpecific Container For User from controller');
                }
             );

	    };


	    self.submitContainerForm = function() {
			  self.createContainer(self.container);
	      };

	    self.createContainer = function(container){
	    	$http.post( REST_SERVICE_URI +'coreController/newcontainer/', container).then(
				              function(response){
                        self.containersHierarchyForUser.push(container);
				            	  self.reset();
								},
				              function(errResponse){
					               console.error('Error while creating Controller (from controller).');
				              }
	            );
	    };

	    self.deleteContainer = function(id){
	    	$http.delete( REST_SERVICE_URI +'coreController/deleteContainer/'+id).then(
				              self.fetchAllContainers,
				              function(errResponse){
					               console.error('Error while deleting Container.');
				              }
	            );
	    };

      self.submitSystemInstanceForm = function() {
        $http.post( REST_SERVICE_URI +'coreController/newSystemInstance/', self.systemInstance).then(
				              function(response){
                        self.systemInstance={id:null, name:'', parentContainer:null, containerType:null, startDate:'', address:'', suspended:''};
								},
				              function(errResponse){
					               console.error('Error while creating System Instance (from controller).');
				              }
	            );
	      };

        self.fetchAllSystemInstances = function(){
  	    	$http.get( REST_SERVICE_URI +'coreController/allSystemInstances/').then(
  						       function(d) {
  							        self.systemInstancesList = d.data;
  						       },
  	      					function(errResponse){
  	      						console.error('error while fetching all containers from controller');
  	      					}
  				       );
  	    };

        self.initSystemInstanceForm = function(){
          if ($stateParams.updateSystemInstance != undefined) {
            self.updateSystemInstancesTypesFiltered($stateParams.updateSystemInstance.parentContainer);
            self.systemInstance = $stateParams.updateSystemInstance;
          }
          $http.get( REST_SERVICE_URI +'coreController/getSystemInstanceById/'+self.currentContainer.parentContainer.id).then(
                     function(resp1) {
                        self.systemInstanceChildsList.push(resp1.data);
                        $http.get( REST_SERVICE_URI +'coreController/allChildsSystemInstances/'+self.currentContainer.parentContainer.id).then(
                                   function(resp2) {
                                     for (var i = 0; i < resp2.data.length; i++) {
                                       self.systemInstanceChildsList.push(resp2.data[i]);
                                     }
                                   },
                                  function(errResponse){
                                    console.error('Error while getting containers Types RelatedToSpecific Container For User from controller');
                                  }
                               );
                     },
                    function(errResponse){
                      console.error('error while fetching system instance by id from controller');
                    }
                 );

        };

        self.updateSystemInstancesTypesFiltered = function(parentContainer){
          self.containerTypesRelatedToSpecificContainer = [];
          self.systemInstance.containerType = null;
          $http.get( REST_SERVICE_URI +'coreController/getContainerTypesRelatedToSpecificContainer/'+parentContainer.id).then(
                     function(d) {
                       self.systemInstanceTypesRelatedToSpecificContainer = d.data;
                     },
                    function(errResponse){
                      console.error('Error while getting System Instances Types RelatedToSpecific Container For User from controller');
                    }
                 );
  	    };

        self.initNewContainerGoals = function(){
          $http.get( REST_SERVICE_URI +'coreController/getContainerGoals/'+self.currentContainer.parentContainer.id).then(
                     function(d) {
                        self.parentContainerGoalsList = [];
                        $scope.allGoals = [];
                        $scope.allGoals = d.data;
                        if($stateParams.assignNewGoalToContainer != undefined){
                        for (var i = 0; i < $scope.allGoals.length; i++) {
                          if($scope.allGoals[i].container.id == $stateParams.assignNewGoalToContainer.parentContainer.id){
                            self.parentContainerGoalsList.push($scope.allGoals[i]);
                          }
                        }
                      }else{
                        for (var i = 0; i < $scope.allGoals.length; i++) {
                          if($scope.allGoals[i].container.id == self.currentContainer.parentContainer.id){
                            self.parentContainerGoalsList.push($scope.allGoals[i]);
                          }
                        }
                      }
                     },
                    function(errResponse){
                      console.error('error while fetching container goals from controller');
                    }
                 );
        };

        self.saveContainerGoal= function(){
          self.goal.container = self.currentContainer;
          $http.post( REST_SERVICE_URI +'coreController/newContainerGoal/', self.goal).then(
  				              function(response){
                          $scope.allGoals.push(self.goal);
                          self.goal={};
  								},
  				              function(errResponse){
  					               console.error('Error while creating Container Goal from controller.');
  				              }
  	            );
        };


        self.initCourseForm = function(){
          if ($stateParams.updateCourseID != undefined) {
            $http.get( REST_SERVICE_URI +'coreController/getCourseById/'+$stateParams.updateCourseID).then(
    						       function(d) {
    							        self.course = d.data;
    						       },
    	      					function(errResponse){
    	      						console.error('error while fetching Course by id from controller');
    	      					}
    				       );
          }
        };

        self.configureCourseTree = function(){
          $http.get( REST_SERVICE_URI +'coreController/listContainerContents/'+$stateParams.createCourseTree.id).then(
                     function(d) {
                       for (var i = 0; i < d.data.length; i++) {
                         var node = {id:d.data[i].id, parent:d.data[i].parent==null?"#":d.data[i].parent, text:d.data[i].name};
                         self.jstreeData.push(node)
                       }
                       $('#courseTree').jstree({
                         "ui": {
                         "select_limit":1,
                         },
                         'plugins': ["wholerow"],
                             'core': {
                               "check_callback" : true,
                                 "themes" : {
                                     "responsive": true
                                 },
                                 data : self.jstreeData
                             },
                             "types" : {
                                 "default" : {
                                     "icon" : "fa fa-folder icon-state-warning icon-lg"
                                 },
                                 "file" : {
                                     "icon" : "fa fa-file icon-state-warning icon-lg"
                                 }
                             },
                         });

                         var _selectedNodeId;
                         $("#courseTree").on("select_node.jstree", function (e, _data) {
                             if ( _selectedNodeId === _data.node.id ) {
                                 _data.instance.deselect_node(_data.node);
                                 _selectedNodeId = "";
                             } else {
                                 _selectedNodeId = _data.node.id;
                             }
                         }).jstree();
                     },
                    function(errResponse){
                      console.error('Error while List Container Contents from controller');
                    }
                 );



            // apply only one single selection
            // $('#courseTree').on('changed.jstree', function (e, data) {
            //   var i, j, r = [];
            //   for(i = 0, j = data.selected.length; i < j; i++) {
            //     r.push(data.instance.get_node(data.selected[i]).text);
            //     if(i>0){
            //       data.instance.deselect_node( [ data.selected[0] ] );
            //     }
            //   }
            // }).jstree();


        };

        self.saveCourseContent = function() {
          var tree = $("#courseTree").jstree(true).get_json();
          $http.post( REST_SERVICE_URI +'coreController/newCourseContent/'+$stateParams.createCourseTree.id, tree).then(
				              function(response){
								},
				              function(errResponse){
					               console.error('Error while creating Course Content (from controller).');
				              }
	            );
        };


        self.showNewTreeNodeDialog = function(){
          self.node = {};
          $mdDialog.show({
              contentElement: '#courseNewTreeNode',
              clickOutsideToClose:true,
              parent:angular.element(document.body),
              fullscreen: $scope.customFullscreen
            });
        };

        self.showEditTreeNodeDialog = function(){
          var ref = $('#courseTree').jstree(true),
                //actual node being deleted
                sel = ref.get_selected();
                if(!sel.length) { return false; }
                var selNode = ref.get_node(sel);
                self.node.name = selNode.text;
          $mdDialog.show({
              contentElement: '#courseEditTreeNode',
              clickOutsideToClose:true,
              parent:angular.element(document.body),
              fullscreen: $scope.customFullscreen
            });
        };

        self.newTreeNode = function() {
          if (self.node.name != undefined) {
            var ref = $('#courseTree').jstree(true),
                //actual node being deleted
                sel = ref.get_selected();
            if(!sel.length) {
               parent = "#";
             }else{
               parent = ref.get_node(sel);
             }
            var position = 'inside';
            var newNode = { state: "open", data: self.node.name };
            $('#courseTree').jstree('create_node', parent, self.node.name, 'last');
            // $('#courseTree').jstree("create_node", parent, position, newNode, false, false);
            $mdDialog.hide();
            self.node = {};
          }
          self.jstreeData = $("#courseTree").jstree(true).get_json();
          $('#courseTree').jstree(true).settings.core.data = $("#courseTree").jstree(true).get_json();
          console.log($("#courseTree").jstree(true).get_json());
          console.log($('#courseTree').jstree(true).settings.core.data);
        };

        self.editTreeNode = function() {
          if (self.node.name != undefined) {
            var ref = $('#courseTree').jstree(true),
                //actual node being deleted
                sel = ref.get_selected();
            if(!sel.length) {
               return false;
             }
            // deletes one tier below selected
            var selNode = ref.get_node(sel);
            $("#courseTree").jstree('rename_node', selNode , self.node.name );
            $mdDialog.hide();
            self.node = {};
          }
          self.jstreeData = $("#courseTree").jstree(true).get_json();
          $('#courseTree').jstree(true).settings.core.data = $("#courseTree").jstree(true).get_json();
          console.log($("#courseTree").jstree(true).get_json());
          console.log($('#courseTree').jstree(true).settings.core.data);
        };

        self.deleteTreeNode = function() {
          var ref = $('#courseTree').jstree(true),
              //actual node being deleted
              sel = ref.get_selected();
          if(!sel.length) { return false; }
          // deletes one tier below selected
          var selNode = ref.get_node(sel);
          ref.delete_node(selNode.children);
          ref.delete_node(selNode);
          $("#courseTree").jstree("remove",selNode.id);
          //deletes selected
          // $("#courseTree").jstree("refresh");
          self.jstreeData = $("#courseTree").jstree(true).get_json();
          $('#courseTree').jstree(true).settings.core.data = $("#courseTree").jstree(true).get_json();
          console.log($("#courseTree").jstree(true).get_json());
          console.log($('#courseTree').jstree(true).settings.core.data);
        };

        self.submitCourseForm = function() {
        self.course.parentContainer = {id: self.currentContainer.id};
        $http.post( REST_SERVICE_URI +'coreController/newCourse/', self.course).then(
				              function(response){
                        self.course={id:null, name:'', parentContainer:null, containerType:null, startDate:'', address:'', suspended:''};
								},
				              function(errResponse){
					               console.error('Error while creating Course (from controller).');
				              }
	            );
	      };

        self.fetchAllCourses = function(){
  	    	$http.get( REST_SERVICE_URI +'coreController/allCourses/'+self.currentContainer.id).then(
  						       function(d) {
  							        self.coursesList = d.data;
  						       },
  	      					function(errResponse){
  	      						console.error('error while fetching all Courses from controller');
  	      					}
  				       );
  	    };

	    // self.remove = function(id){
	  	//   var box = $("#mb-remove-row");
	    //     box.addClass("open");
	    //     box.find(".mb-control-yes").on("click",function(){
	    //         box.removeClass("open");
	    //         self.deleteContainer(id);
	    //     });
	    //     box.find(".mb-control-close").on("click",function(){
	    //         box.removeClass("open");
	    //     });
	    // };

	    self.reset = function(){
	  	  self.container={id:null, name:'', parentContainer:null, containerType:'', address:'', suspended:''};
	        $scope.myForm.$setPristine(); //reset Form
	    };




//	      Date Picker

	      $scope.today = function()
	        {
	            $scope.dt = new Date();
	        };
	        $scope.today();
	        $scope.clear = function()
	        {
	            $scope.dt = null;
	        };
	        $scope.inlineOptions = {
	            customClass: getDayClass,
	            minDate: new Date(),
	            showWeeks: true
	        };
	        $scope.dateOptions = {
//	             dateDisabled: disabled,
	            formatYear: 'yy',
	            maxDate: new Date(2020, 5, 22),
	            minDate: new Date(),
	            startingDay: 1
	        };
	        // Disable weekend selection
	        function disabled(data)
	        {
	            var date = data.date,
	                mode = data.mode;
	            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
	        }
	        $scope.toggleMin = function()
	        {
	            $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
	            $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
	        };
	        $scope.toggleMin();
	        $scope.open1 = function()
	        {
	            $scope.popup1.opened = true;
	        };
	        $scope.open2 = function()
	        {
	            $scope.popup2.opened = true;
	        };
	        $scope.setDate = function(year, month, day)
	        {
	            $scope.dt = new Date(year, month, day);
	        };
	        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
	        $scope.format = $scope.formats[0];
	        $scope.altInputFormats = ['M!/d!/yyyy'];
	        $scope.popup1 = {
	            opened: false
	        };
	        $scope.popup2 = {
	            opened: false
	        };
	        var tomorrow = new Date();
	        tomorrow.setDate(tomorrow.getDate() + 1);
	        var afterTomorrow = new Date();
	        afterTomorrow.setDate(tomorrow.getDate() + 1);
	        $scope.events = [
	        {
	            date: tomorrow,
	            status: 'full'
	        },
	        {
	            date: afterTomorrow,
	            status: 'partially'
	        }];

	        function getDayClass(data)
	        {
	            var date = data.date,
	                mode = data.mode;
	            if (mode === 'day')
	            {
	                var dayToCheck = new Date(date).setHours(0, 0, 0, 0);
	                for (var i = 0; i < $scope.events.length; i++)
	                {
	                    var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);
	                    if (dayToCheck === currentDay)
	                    {
	                        return $scope.events[i].status;
	                    }
	                }
	            }
	            return '';
	        }
  }

})();
