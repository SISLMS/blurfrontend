

(function () {
  'use strict';

  angular.module('BlurAdmin.pages.containers', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('dashboard.containers', {
          url: '/containers',
          template : '<ui-view  autoscroll="true" autoscroll-body-top></ui-view>',
          abstract: true,
          controller: 'ContainersPageCtrl',
          title: 'Container Mngmnt',
          sidebarMeta: {
            icon: 'ion-grid',
            order: 300,
          },
        }).state('dashboard.containers.newSystemInstanceType', {
            url: '/newSystemInstanceType',
            templateUrl: 'app/pages/containers/new/newSystemInstanceType.html',
            title: 'New Sys Ins Type',
            controller: 'ContainerTypesPageCtrl',
            sidebarMeta: {
              order: 0,
            },
          }).state('dashboard.containers.listSystemInstanceTypes', {
              url: '/listSystemInstanceTypes',
              templateUrl: 'app/pages/containers/list/listSystemInstanceTypes.html',
              title: 'List Sys Ins Type',
              controller: 'ContainerTypesPageCtrl',
              sidebarMeta: {
                order: 0,
              },
          }).state('dashboard.containers.updateSystemInstanceType', {
            url: '/updateSystemInstanceType',
            templateUrl: 'app/pages/containers/new/newSystemInstanceType.html',
            title: 'Update Sys Ins Type',
            controller: "ContainerTypesPageCtrl",
            params: {
                updateSystemInstanceType: "updateSystemInstanceType"
             }
          }).state('dashboard.containers.newSystemInstance', {
              url: '/newSystemInstance',
              templateUrl: 'app/pages/containers/new/newSystemInstance.html',
              title: 'New Sys Inst',
              controller: 'ContainersPageCtrl',
              sidebarMeta: {
                order: 0,
               },
           }).state('dashboard.containers.listSystemInstances', {
               url: '/listSystemInstances',
               templateUrl: 'app/pages/containers/list/listSystemInstances.html',
               title: 'List System Instances',
               controller: 'ContainersPageCtrl',
               sidebarMeta: {
                 order: 0,
               },
           }).state('dashboard.containers.updateSystemInstance', {
               url: '/updateSystemInstance',
               templateUrl: 'app/pages/containers/new/newSystemInstance.html',
               title: 'School Update System Instance',
               controller: "ContainersPageCtrl",
               params: {
                   updateSystemInstance: "updateSystemInstance"
               }
           }).state('dashboard.containers.systemInstanceGoals', {
               url: '/systemInstanceGoals',
               templateUrl: 'app/pages/containers/new/containerGoals.html',
               title: 'System Instance Goals',
               controller: "ContainersPageCtrl",
           }).state('dashboard.containers.newClassRoom', {
               url: '/newClassRoom',
               templateUrl: 'app/pages/containers/new/newClassRoom.html',
               title: 'New Class Room',
               controller: 'ClassRoomCtrl',
               sidebarMeta: {
                 order: 0,
                },
            }).state('dashboard.containers.listClassRooms', {
                url: '/listClassRooms',
                templateUrl: 'app/pages/containers/list/listClassRooms.html',
                title: 'List Class Rooms',
                controller: 'ClassRoomCtrl',
                sidebarMeta: {
                  order: 0,
                },
            }).state('dashboard.containers.updateClassRoom', {
                url: '/updateClassRoom',
                templateUrl: 'app/pages/containers/new/newClassRoom.html',
                title: 'Update Class Room',
                controller: "ClassRoomCtrl",
                params: {
                    updateClassRoom: "updateClassRoom"
                }
            }).state('dashboard.containers.schedualClassRoom', {
                url: '/schedualClassRoom',
                templateUrl: 'app/pages/containers/new/classRoomSchedual.html',
                title: 'Update Class Room',
                controller: "ClassRoomCtrl",
                params: {
                    schedualClassRoomId: "schedualClassRoomId"
                }
            }).state('dashboard.containers.schedualClassRoomSections', {
                url: '/classRoomSectionProps',
                templateUrl: 'app/pages/containers/new/classRoomSectionProps.html',
                title: 'Class Room Section Props',
                controller: "ClassRoomCtrl",
                params: {
                    setSectionPropsByClassRoomId: "setSectionPropsByClassRoomId"
                }
            }).state('dashboard.containers.newCourse', {
               url: '/newCourse',
               templateUrl: 'app/pages/containers/new/newCourse.html',
               title: 'New Course',
               controller: 'ContainersPageCtrl',
               sidebarMeta: {
                 order: 0,
                },
            }).state('dashboard.containers.listCourses', {
                url: '/listCourses',
                templateUrl: 'app/pages/containers/list/listCourses.html',
                title: 'List Course',
                controller: 'ContainersPageCtrl',
                sidebarMeta: {
                  order: 0,
                },
            }).state('dashboard.containers.updateCourse', {
                url: '/updateCourse',
                templateUrl: 'app/pages/containers/new/newCourse.html',
                title: 'Update Course',
                controller: "ContainersPageCtrl",
                params: {
                    updateCourseID: "updateCourseID"
                }
            }).state('dashboard.containers.containerGoals', {
                url: '/containerGoals',
                templateUrl: 'app/pages/containers/new/containerGoals.html',
                title: 'Set Container Goals',
                controller: "ContainersPageCtrl",
                sidebarMeta: {
                  order: 0,
                },
            }).state('dashboard.containers.courseGoals', {
                url: '/courseGoals',
                templateUrl: 'app/pages/containers/new/containerGoals.html',
                title: 'Set Course Goals',
                controller: "ContainersPageCtrl",
                params: {
                    assignNewGoalToContainer: "assignNewGoalToContainer"
                }
            }).state('dashboard.containers.courseTree', {
                url: '/courseTree',
                templateUrl: 'app/pages/containers/new/courseTree.html',
                title: 'Set Course Tree',
                controller: "ContainersPageCtrl",
                params: {
                    createCourseTree: "createCourseTree"
                }
            });
    $urlRouterProvider.when('/containers', '/containers/newSystemInstance','/containers/listSystemInstances'
    ,'/containers/newClassRoom','/containers/listClassRooms','/containers/containerGoals');
  }

})();



// '/containers/listContainers',
// '/containers/newContainer',
// '/containers/updateContainer',

// .state('dashboard.containers.updateContainer', {
//     url: '/updateContainer',
//     templateUrl: 'app/pages/containers/new/newContainer.html',
//     title: 'School Update Container',
//     controller: "ContainersPageCtrl",
//     params: {
//         updateContainer: "updateContainer"
//     }
// }).state('dashboard.containers.newContainer', {
//     url: '/newContainer',
//     templateUrl: 'app/pages/containers/new/newContainer.html',
//     title: 'New Container',
//     controller: 'ContainersPageCtrl',
//     sidebarMeta: {
//       order: 0,
//      },
//  }).state('dashboard.containers.listContainers', {
//      url: '/listContainers',
//      templateUrl: 'app/pages/containers/list/listContainers.html',
//      title: 'List Containers',
//      controller: 'ContainersPageCtrl',
//      sidebarMeta: {
//        order: 0,
//      },
//    }).state('dashboard.containers.newContainerType', {
//   url: '/newContainerType',
//   templateUrl: 'app/pages/containers/new/newContainerType.html',
//   title: 'New ContainerType',
//   controller: 'ContainerTypesPageCtrl',
//   sidebarMeta: {
//     order: 0,
//   },
// }).state('dashboard.containers.listContainerTypes', {
//     url: '/listContainerTypes',
//     templateUrl: 'app/pages/containers/list/listContainerTypes.html',
//     title: 'List ContainerTypes',
//     controller: 'ContainerTypesPageCtrl',
//     sidebarMeta: {
//       order: 0,
//     },
// }).state('dashboard.containers.updateContainerType', {
//     url: '/updateContainerType',
//     templateUrl: 'app/pages/containers/new/newContainerType.html',
//     title: 'Update Container Type',
//     controller: "ContainerTypesPageCtrl",
//     params: {
//         updateContainerType: "updateContainerType"
//      }
//   })
