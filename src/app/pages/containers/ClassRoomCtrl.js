

(function () {
  'use strict';

  angular.module('BlurAdmin.pages.containers')
      .controller('ClassRoomCtrl', ClassRoomCtrl)
      .directive('afterRender', ['$timeout', function ($timeout) {
  	    var def = {
  	        restrict: 'A',
  	        terminal: true,
  	        transclude: false,
  	        link: function (scope, element, attrs) {
  	        	$timeout(scope.$eval(attrs.afterRender), 0);  //Calling a scoped method

  	        }
  	    };
  	    return def;
  	   }]);

  /** @ngInject */
  function ClassRoomCtrl($scope, $filter, $http, $stateParams, $state, editableOptions, $mdDialog, $uibModal, editableThemes) {

	  var REST_SERVICE_URI = 'http://localhost:8081/BackEnd/api/';
	  var loggedInUserId = 52;

	    var self = this;
      self.classRoom={id:null, name:null, parentContainer:null, containerType:null, startDate:'', address:'', suspended:'', users:[]};
      self.classRoomsList=[];
      self.test=[];
      $scope.containerUsers = [{id:null, name:'', containerRoles:[]}];
      $scope.classRoomUsers = [{id:null, name:'', containerRoles:[]}];
      // $scope.sectionUsers = [{id:null, name:'', containerRoles:[], courses:[]}];
      $scope.sectionUsers = [];
      self.selectedSchedualStructure = {};
      self.schedualStructureList = [];
      $scope.initialData=[];
      self.schedualPeriodType = {text:null, id:null, color:null};
      self.schedualPeriodTypeArrayOnLoad=[];
      self.schedualStructure = {id:null, name:''};
      self.coursesList = [];
      self.section = {};
      self.sectionCourse = {};
      self.jstreeData = [];
      self.currentClassRoomContainer = {id:4, parentContainer:{id:3}};
      self.currentSectionContainer = {id:5, parentContainer:{id:4}};

      self.getSchedualStructureTypes = function(){
        $http.get( REST_SERVICE_URI +'schedualStructureController/SchedualPeriodTypes/').then(
                    function(response){
                      for (var i = 0; i < response.data.length; i++) {
                        // self.schedualPeriodType = {text:null, id:null, color:null};
                        self.schedualPeriodType = {text:response.data[i].name, id:response.data[i].id, color:response.data[i].color};
                        self.schedualPeriodTypeArrayOnLoad.push(self.schedualPeriodType);
                      }
                },
                    function(errResponse){
                       console.error('Error while getting Schedual Period Types .');
                      }
               );
      };

      self.getSchedualStructureTypes();

        self.initClassRoomSchedualForm = function(){
          // self.assignMySchedualStartTime();
          $http.get( REST_SERVICE_URI +'schedualStructureController/getAllSchedualStructures/').then(
  						       function(d) {
  							        self.schedualStructureList = d.data;
  						       },
  	      					function(errResponse){
  	      						console.error('error while fetching all Schedual Structures controller');
  	      					}
  				       );



        };

        self.updateSchedual = function(){
          if ($stateParams.schedualClassRoomId.id != undefined) {
          $stateParams.selectedSchedualStructureId = self.selectedSchedualStructure.id;
          $http.get( REST_SERVICE_URI +'schedualStructureController/getTermStartAndEnd/').then(
                      function(response){
                        // console.log(new Date(2019, 10, 4));
                        console.log(new Date(response.data[0]));
                        $("#schedulerContainerCreateSection").dxScheduler({
                          currentDate: new Date(response.data[0])
                        });
                        $scope.missionCompled();
                        $http.get( REST_SERVICE_URI +'schedualStructureController/getSchedualStructurePeriodsRelatedToContainer?classRoomId='+$stateParams.schedualClassRoomId.id+'&schedualStructureID='+self.selectedSchedualStructure.id).then(
                                    function(res){
                                       $scope.initialData = [];
                                       self.schedualStructure = res.data[0].schedualStructure;
                                       var scheduler = $("#schedulerContainer").dxScheduler("instance");
                                      for (var i = 0; i < res.data.length; i++) {
                                       // scheduler.addAppointment({id:res.data[i].id, text: res.data[i].name, schedualStructure:{id:schedualStructure.id, name:schedualStructure.name}, ownerId: res.data[i].schedualPeriodType.id,
                                       //   startDate: new Date(res.data[i].startDate), endDate: new Date(res.data[i].endDate)});
                                        $scope.initialData.push({id:res.data[i].id, text: res.data[i].name, schedualStructure:{id:self.schedualStructure.id, name:self.schedualStructure.name},
                                          ownerId: res.data[i].schedualPeriodType.id, startDate: new Date(res.data[i].startDate), endDate: new Date(res.data[i].endDate)});
                                      }

                                     //use addAppointment() to style view
                                     $("#schedulerContainerCreateSection").dxScheduler({
                                       dataSource: $scope.initialData
                                     });

                                     // scheduler.addAppointment({text: "sssssssss",  ownerId: 2, startDate: new Date(2015, 4, 26, 9, 30), endDate: new Date(2015, 4, 26, 11, 30)});
                                     // scheduler.deleteAppointment({text: "sssssssss",  ownerId: 2, startDate: new Date(2015, 4, 26, 9, 30), endDate: new Date(2015, 4, 26, 11, 30)});
                                     self.schedualStructure = res.data[0].schedualStructure;
                                },
                                    function(errResponse){
                                       console.error('Error while getting All Schedual Structure Periods .');
                                      }
                               );
                  },
                      function(errResponse){
                         console.error('Error while getting All Schedual Structure .');
                        }
                 );

          };
        };

        self.updateSchedualSections = function(){
          if ($stateParams.setSectionPropsByClassRoomId != undefined) {
          $stateParams.selectedSchedualStructureId = self.selectedSchedualStructure.id;
          self.getSchedualStructurePeriodSections($stateParams.setSectionPropsByClassRoomId, self.selectedSchedualStructure.id);
          };
        };

        self.getSchedualStructurePeriods = function(classRoomId, schedualStructureID){
             $http.get( REST_SERVICE_URI +'schedualStructureController/getSchedualStructurePeriodsRelatedToContainer?classRoomId='+classRoomId+'&schedualStructureID='+schedualStructureID).then(
                         function(res){
                            $scope.initialData = [];
                            self.schedualStructure = res.data[0].schedualStructure;
                            var scheduler = $("#schedulerContainer").dxScheduler("instance");
                           for (var i = 0; i < res.data.length; i++) {
                            // scheduler.addAppointment({id:res.data[i].id, text: res.data[i].name, schedualStructure:{id:schedualStructure.id, name:schedualStructure.name}, ownerId: res.data[i].schedualPeriodType.id,
                            //   startDate: new Date(res.data[i].startDate), endDate: new Date(res.data[i].endDate)});
                             $scope.initialData.push({id:res.data[i].id, text: res.data[i].name, schedualStructure:{id:self.schedualStructure.id, name:self.schedualStructure.name},
                               ownerId: res.data[i].schedualPeriodType.id, startDate: new Date(res.data[i].startDate), endDate: new Date(res.data[i].endDate)});
                           }

                          //use addAppointment() to style view
                          $("#schedulerContainerCreateSection").dxScheduler({
                            dataSource: $scope.initialData
                          });

                          // scheduler.addAppointment({text: "sssssssss",  ownerId: 2, startDate: new Date(2015, 4, 26, 9, 30), endDate: new Date(2015, 4, 26, 11, 30)});
                          // scheduler.deleteAppointment({text: "sssssssss",  ownerId: 2, startDate: new Date(2015, 4, 26, 9, 30), endDate: new Date(2015, 4, 26, 11, 30)});
                          self.schedualStructure = res.data[0].schedualStructure;
                     },
                         function(errResponse){
                            console.error('Error while getting All Schedual Structure Periods .');
                           }
                    );
        };



      self.getSchedualStructurePeriodSections = function(classRoomId, schedualStructureID){
           $http.get( REST_SERVICE_URI +'schedualStructureController/getSchedualStructurePeriodsSection?classRoomId='+classRoomId+'&schedualStructureID='+schedualStructureID).then(
                       function(res){
                          $scope.initialData = [];
                          self.schedualStructure = res.data[0].schedualStructure;
                          var scheduler2 = $("#schedulerClassRoomSectionProps").dxScheduler("instance");
                           for (var i = 0; i < res.data.length; i++) {
                            // scheduler.addAppointment({id:res.data[i].id, text: res.data[i].name, schedualStructure:{id:schedualStructure.id, name:schedualStructure.name}, ownerId: res.data[i].schedualPeriodType.id,
                            //   startDate: new Date(res.data[i].startDate), endDate: new Date(res.data[i].endDate)});
                             $scope.initialData.push({id:res.data[i].id, text: res.data[i].name, schedualStructure:{id:self.schedualStructure.id, name:self.schedualStructure.name},
                               ownerId: res.data[i].periodType.id, period: res.data[i].period, startDate: new Date(res.data[i].startDate), endDate: new Date(res.data[i].endDate)});
                           }

                        if($stateParams.setSectionPropsByClassRoomId != undefined){
                          $("#schedulerClassRoomSectionProps").dxScheduler({
                            dataSource: $scope.initialData
                          });
                        }
                        // scheduler.addAppointment({text: "sssssssss",  ownerId: 2, startDate: new Date(2015, 4, 26, 9, 30), endDate: new Date(2015, 4, 26, 11, 30)});
                        // scheduler.deleteAppointment({text: "sssssssss",  ownerId: 2, startDate: new Date(2015, 4, 26, 9, 30), endDate: new Date(2015, 4, 26, 11, 30)});
                        self.schedualStructure = res.data[0].schedualStructure;
                   },
                       function(errResponse){
                          console.error('Error while getting All Schedual Structure Periods sections .');
                         }
                  );
      };


        self.checkIfSelected = function(array, object){
          for (var i = 0; i < array.length; i++) {
            if(array[i].id == object.id)
            return true;
          }
        };

        self.initClassRoomForm = function(){
          if ($stateParams.updateClassRoom != undefined) {
            $http.get( REST_SERVICE_URI +'coreController/getClassRoomById/'+$stateParams.updateClassRoom.id).then(
    						       function(d) {
    							        self.classRoom = d.data;
                          $scope.containerUsers = [];
                          $scope.containerUsers = self.classRoom.users;
                                        $http.get( REST_SERVICE_URI +'coreController/getUsersAssignedToContainer/'+self.classRoom.parentContainer.id).then(
                                        function(res) {
                                          for (var i = 0; i < res.data.length; i++) {
                                          for (var x = 0; x < $scope.containerUsers.length; x++) {
                                            if ($scope.containerUsers[x].id == res.data[i].id) {
                                              $scope.containerUsers[x].containerRoles = [];
                                              $scope.containerUsers[x].userRolesInParentContainer = res.data[i].containerRoles;
                                          }
                                          }
                                          }
                                          for (var i = 0; i < res.data.length; i++) {
                                            var adding = true;
                                            for (var x = 0; x < $scope.containerUsers.length; x++) {
                                              if($scope.containerUsers[x].id == res.data[i].id){
                                                adding = false;
                                                }
                                              }
                                              if (adding) {
                                                res.data[i].userRolesInParentContainer = res.data[i].containerRoles;
                                                res.data[i].containerRoles = [];
                                                $scope.containerUsers.push(res.data[i]);
                                              }
                                              }
                                              },
                                              function(errResponse){
                                                console.error('Error while fetching Users Assigned To Container from controller');
                                                }
                                            );
    						       },
    	      					function(errResponse){
    	      						console.error('error while fetching system instance by id from controller');
    	      					}
    				       );
          }

          if ($stateParams.updateClassRoom == undefined) {
          $http.get( REST_SERVICE_URI +'coreController/getUsersAssignedToContainer/'+3).then(
                     function(d) {
                        $scope.containerUsers = d.data;
                        for (var i = 0; i < $scope.containerUsers.length; i++) {
                          $scope.containerUsers[i].userRolesInParentContainer = $scope.containerUsers[i].containerRoles;
                          $scope.containerUsers[i].containerRoles = [];
                        }
                     },
                    function(errResponse){
                      console.error('Error while fetching Users Assigned To Container from controller');
                    }
                 );



          // $http.get( REST_SERVICE_URI +'coreController/getContainerRoles/'+self.currentClassRoomContainer.id).then(
          //            function(d) {
          //               self.containerRolesList = d.data;
          //            },
          //           function(errResponse){
          //             console.error('Error while fetching Container Roles from controller');
          //           }
          //        );
               }

        };

        self.submitClassRoomForm = function() {
        self.classRoom.parentContainer = self.currentClassRoomContainer.parentContainer;
        self.classRoom.users = [];
        for (var i = 0; i < $scope.containerUsers.length; i++) {
          if ($scope.containerUsers[i].containerRoles != undefined && $scope.containerUsers[i].containerRoles.length>0) {
            self.classRoom.users.push($scope.containerUsers[i]);
          }
        }
        $http.post( REST_SERVICE_URI +'coreController/newClassRoom/', self.classRoom).then(
				              function(response){
                        self.classRoom={id:null, name:'', parentContainer:null, containerType:null, startDate:'', address:'', suspended:'',users:[]};
                        for (var i = 0; i < $scope.containerUsers.length; i++) {
                          if ($scope.containerUsers[i].containerRoles.length > 0) {
	                        	    $scope.containerUsers[i].containerRoles = [];
	                        }
                        }
								},
				              function(errResponse){
					               console.error('Error while creating System Instance (from controller).');
				              }
	            );
	      };

        self.fetchAllClassRooms = function(){
  	    	$http.get( REST_SERVICE_URI +'coreController/allClassRooms/').then(
  						       function(d) {
  							        self.classRoomsList = d.data;
  						       },
  	      					function(errResponse){
  	      						console.error('error while fetching all Class Rooms from controller');
  	      					}
  				       );
  	    };

        self.getCourseSection= function(){
          $http.get( REST_SERVICE_URI +'coreController/getCourseSection/'+$stateParams.schedualClassRoomId.id +'/'+ $stateParams.selectedSchedualStructureId+'/'+self.sectionCourse.id).then(
                    function(d) {
                        self.section = d.data;
                        if(self.section.sectionUsers != undefined){
                        $scope.sectionUsers = self.section.sectionUsers;
                        }
                        $http.get( REST_SERVICE_URI +'coreController/getUsersAssignedToContainer/'+$stateParams.schedualClassRoomId.parentContainer.id).then(
                                   function(d) {
                                      $scope.classRoomUsers = d.data;
                                      for (var i = 0; i < $scope.classRoomUsers.length; i++) {
                                        var insert = true;
                                        for (var x = 0; x < $scope.sectionUsers.length; x++) {
                                          if($scope.sectionUsers[x].id == $scope.classRoomUsers[i].id){
                                            insert = false;
                                          }
                                        }
                                        if(insert){
                                            $scope.classRoomUsers[i].removeRoles = true;
                                            $scope.sectionUsers.push($scope.classRoomUsers[i]);
                                          }
                                      }
                                      for (var i = 0; i < $scope.sectionUsers.length; i++) {
                                        $scope.sectionUsers[i].containerRolesList =  $scope.sectionUsers[i].containerRoles.slice();
                                        if($scope.sectionUsers[i].removeRoles == true)
                                        $scope.sectionUsers[i].containerRoles = [];
                                      }
                                   },
                                  function(errResponse){
                                    console.error('Error while fetching Users Assigned To Container from controller');
                                  }
                               );
                     },
                    function(errResponse){
                       console.error('error while fetching Class Room users from controller');
                    }
                  );

        };



        self.initClassRoomSection= function(){
          $http.get( REST_SERVICE_URI +'coreController/allCourses/'+$stateParams.schedualClassRoomId.parentContainer.id).then(
  						       function(d) {
  							        self.coursesList = d.data;
  						       },
  	      					function(errResponse){
  	      						console.error('error while fetching all Class Room courses from controller');
  	      					}
  				       );
        };

        self.initClassRoomSectionProps= function(){
          $http.get( REST_SERVICE_URI +'coreController/getPeriodSections/'+$stateParams.setSectionPropsByClassRoomId+'/'+ $stateParams.selectedSchedualStructurePeriodInstance.period.id).then(
                     function(response) {
                       self.periodSections = response.data;
                       self.periodSections.containerTableOfContents=[];
                         $http.post( REST_SERVICE_URI +'coreController/listCoursesContentBySection/', self.periodSections).then(
                                    function(response2) {
                                      for (var i = 0; i < self.periodSections.length; i++) {
                                        self.jstreeData = [];
                                         for (var x = 0; x < response2.data.length; x++) {
                                           if(self.periodSections[i].course.id == response2.data[x].container.id ){
                                           self.jstreeData.push(response2.data[x]);
                                         }
                                         }
                                         $('#'+self.periodSections[i].id).jstree({
                                            'plugins': ["wholerow", "checkbox", "types"],
                                                 'core': {
                                                     "themes" : {
                                                         "responsive": false
                                                     },
                                                     data : self.jstreeData
                                                 },
                                                 "types" : {
                                                     "default" : {
                                                         "icon" : "fa fa-folder icon-state-warning icon-lg"
                                                     },
                                                     "file" : {
                                                         "icon" : "fa fa-file icon-state-warning icon-lg"
                                                     }
                                                 },
                                             });
                                      }
                                    },
                                   function(errResponse){
                                     console.error('Error while Listccccc Container Contents from controller');
                                   }
                                );
                     },
                    function(errResponse){
                      console.error('Error while List Get Period Sections');
                    }
                 );
        };
        self.saveOrUpdateClassRoomSectionProps= function(){
          var selectedNodes = [];
          var allSelectedNodes = [];
          for (var i = 0; i < self.periodSections.length; i++) {
            selectedNodes =  $("#"+self.periodSections[i].id).jstree("get_checked",null,true);
            for (var x = 0; x < selectedNodes.length; x++) {
              allSelectedNodes.push({assignedContainer:{id:self.periodSections[i].id}, containerTableOfContent:{id:selectedNodes[x]}});
            }
          }
          $http.post( REST_SERVICE_URI +'coreController/createAssignedTableOfContents/', allSelectedNodes).then(
                     function(response2) {
                       selectedNodes = [];
                       allSelectedNodes = [];
                       self.closeSectionDialog();
                     },
                    function(errResponse){
                      console.error('Error While Create Assigned Table Of Contents from controller');
                    }
                 );
        };


        self.saveOrUpdateClassRoomSection= function(){
          self.section.sectionUsers =  $scope.sectionUsers;
          self.section.course = self.sectionCourse;
          for (var i = 0; i < self.section.sectionUsers.length; i++) {
            if (self.sectionCourse != undefined) {
              self.section.sectionUsers[i].courses.push(self.sectionCourse);
            }

          }
          $http.post( REST_SERVICE_URI +'coreController/createSection/'+$stateParams.selectedSchedualStructurePeriod.id+'/'+$stateParams.schedualClassRoomId.id, self.section).then(
				              function(response){
                        self.section = {};
                        self.closeSectionDialog();

								},
				              function(errResponse){
					               console.error('Error while creating Section From Controller.');
				              }
	            );
          console.log($scope.sectionUsers.length);
        };

        self.openSectionPropsDialog = function() {
        $mdDialog.show({
            templateUrl: 'app/pages/containers/modals/classRoomSectionPropsDialog.html',
            clickOutsideToClose:true,
            parent:angular.element(document.body),
            fullscreen: $scope.customFullscreen
          });
        };

        self.closeSectionPropsDialog = function() {
          $mdDialog.hide();
        };

        self.openSectionDialog = function() {
        $mdDialog.show({
            templateUrl: 'app/pages/containers/modals/classRoomSection.html',
            clickOutsideToClose:true,
            parent:angular.element(document.body),
            fullscreen: $scope.customFullscreen
          });
        };

        self.closeSectionDialog = function() {
          $mdDialog.hide();
        };

        self.open = function (page, size) {
          $uibModal.open({
            animation: true,
            templateUrl: page,
            size: size,
            resolve: {
              items: function () {
                return $scope.items;
              }
            }
          });
        };

        $scope.createClassRoomSection = function(e) {
          $stateParams.selectedSchedualStructurePeriod = e.appointmentData;

          self.openSectionDialog();

          e.cancel = $.Deferred();

          // self.open('app/pages/containers/modals/classRoomSection.html', 'lg');
  	      };

        $scope.createClassRoomSectionProps = function(e) {
          $stateParams.selectedSchedualStructurePeriodInstance = e.appointmentData;

          self.openSectionPropsDialog();

          e.cancel = $.Deferred();

          // self.open('app/pages/containers/modals/classRoomSection.html', 'lg');
  	      };

        $scope.initializedHandler = function (e) {
              $scope.editing = {
               allowAdding: false,
               allowUpdating: false,
               allowDeleting: false,
               allowResizing: false,
               allowDragging: false
             };
             $("#schedulerContainerCreateSection").dxScheduler({
               onAppointmentClick : $scope.createClassRoomSection,
               onAppointmentDblClick : $scope.createClassRoomSection
             });

             $("#schedulerClassRoomSectionProps").dxScheduler({
               onAppointmentClick : $scope.createClassRoomSectionProps,
               onAppointmentDblClick : $scope.createClassRoomSectionProps
             });


        };

        self.assignMySchedualStartTime = function(){
          $http.get( REST_SERVICE_URI +'schedualStructureController/getTermStartAndEnd/').then(
                      function(response){
                        console.log(response.data[0]);
                        if ($stateParams.schedualClassRoomId.id != undefined) {
                          $("#schedulerContainer").dxScheduler({
                            currentDate: new Date(response.data[0])
                          });
                          $scope.missionCompled();
                        } else if($stateParams.setSectionPropsByClassRoomId != undefined){
                          $("#schedulerClassRoomSectionProps").dxScheduler({
                            currentDate: new Date(response.data[0])
                          });
                          $scope.schedulerClassRoomSectionPropsMissionCompled();
                        }


                  },
                      function(errResponse){
                         console.error('Error while getting All Schedual Structure .');
                        }
                 );
        };

        $scope.missionCompled = function()
  	    {
  	    	//alert(document.getElementsByClassName("dx-scheduler-header-panel-cell").length)
  	    var slides = document.getElementsByClassName("dx-scheduler-header-panel-cell");
  	    for(var i = 0; i < slides.length; i++)
  	    {
  	       slides.item(i).innerHTML= slides.item(i).innerHTML.replace(/\d+/g, '');;
  	    }
  	    var slides = document.getElementsByClassName("dx-scheduler-header");
  	    if(slides.length>0){
  	    	slides.item(0).style.display='none'
  	    }
        var header = document.getElementsByClassName("dx-scheduler-all-day-panel");
        if(header.length>0){
  	    	header.item(0).style.display='none'
  	    }
        var headerTitle = document.getElementsByClassName("dx-scheduler-all-day-title");
        if(headerTitle.length>0){
  	    	headerTitle.item(0).style.display='none'
  	    }
        var options = document.getElementsByClassName("options");
        if(options.length>0){
  	    	options.item(0).style.display='none'
  	    }
  //	    dx-scheduler-header dx-widget
  	    };

        $scope.schedulerClassRoomSectionPropsMissionCompled = function()
  	    {
  	    	//alert(document.getElementsByClassName("dx-scheduler-header-panel-cell").length)
  	    var slides = document.getElementsByClassName("dx-scheduler-header-panel-cell");
  	    for(var i = 0; i < slides.length; i++)
  	    {
  	       slides.item(i).innerHTML= slides.item(i).innerHTML.replace(/\d+/g, '');;
  	    }
  	    // var slides = document.getElementsByClassName("dx-scheduler-header");
  	    // if(slides.length>0){
  	    // 	slides.item(0).style.display='none'
  	    // }
        var header = document.getElementsByClassName("dx-scheduler-all-day-panel");
        if(header.length>0){
  	    	header.item(0).style.display='none'
  	    }
        var headerTitle = document.getElementsByClassName("dx-scheduler-all-day-title");
        if(headerTitle.length>0){
  	    	headerTitle.item(0).style.display='none'
  	    }
        var options = document.getElementsByClassName("options");
        if(options.length>0){
  	    	options.item(0).style.display='none'
  	    }
  //	    dx-scheduler-header dx-widget
  	    };


        $scope.useDropDownViewSwitcher = false;
        $scope.disabledValue = false;

        var switchModeNames = ["Tabs", "Drop-Down Menu"];
        $scope.schedulerOptions = {
            dataSource: $scope.initialData,
            views: ["agenda", "month", "week", "workWeek", "day"],
            currentView: "week",
            useDropDownViewSwitcher: false,
            firstDayOfWeek: 6,
            startDayHour: 8,
            endDayHour: 19,
            rtlEnabled:true,
            onInitialized: $scope.initializedHandler,
            bindingOptions: {
                editing: "editing",
                useDropDownViewSwitcher: "useDropDownViewSwitcher"
            },
            resources: [{
                fieldExpr: "ownerId",
                label:"النوع",
                allowMultiple: false,
                dataSource: self.schedualPeriodTypeArrayOnLoad
            }],
            width: "100%",
            height: 600
        };

        $scope.schedulerClassRoomSectionPropsOptions = {
            dataSource: $scope.initialData,
            views: ["agenda", "month", "week", "workWeek", "day"],
            currentView: "week",
            useDropDownViewSwitcher: false,
            firstDayOfWeek: 6,
            startDayHour: 8,
            endDayHour: 19,
            rtlEnabled:true,
            onInitialized: $scope.initializedHandler,
            bindingOptions: {
                editing: "editing",
                useDropDownViewSwitcher: "useDropDownViewSwitcher"
            },
            resources: [{
                fieldExpr: "ownerId",
                label:"النوع",
                allowMultiple: false,
                dataSource: self.schedualPeriodTypeArrayOnLoad
            }],
            width: "100%",
            height: 600
        };

        $scope.useDropDownViewSwitcherOptions = {
            items: switchModeNames,
            width: 200,
            value: switchModeNames[0],
            onValueChanged: function(data) {
                $scope.useDropDownViewSwitcher = data.value === switchModeNames[1];
            }
        };
        $scope.allowAddingOptions = {
            text: "Allow adding",
            bindingOptions: {
                value: "editing.allowAdding"
            }
        };
        $scope.allowUpdatingOptions = {
            text: "Allow updating",
            onValueChanged: function(data) {
                $scope.disabledValue = !data.value;
            },
            bindingOptions: {
                value: "editing.allowUpdating"
            }
        };
        $scope.allowDeletingOptions = {
            text: "Allow deleting",
            bindingOptions: {
                value: "editing.allowDeleting"
            }
        };
        $scope.allowResizingOptions = {
            text: "Allow resizing",
            bindingOptions: {
                value: "editing.allowResizing",
                disabled: "disabledValue"
            }
        };
        $scope.allowDraggingOptions = {
            text: "Allow dragging",
            bindingOptions: {
                value: "editing.allowDragging",
                disabled: "disabledValue"
            }
        };


  }

})();
