

(function () {
  'use strict';

  angular.module('BlurAdmin.pages.containers')
      .controller('ContainerTypesPageCtrl', ContainerTypesPageCtrl);

  /** @ngInject */
  function ContainerTypesPageCtrl($scope, $filter, $http, $stateParams, editableOptions, editableThemes) {

	  var REST_SERVICE_URI = 'http://localhost:8081/BackEnd/api/';
	  var loggedInUserId = 1;

	    var self = this;
	    self.containerType={id:null, name:'', description:'', parentContainer:''};
	    self.containerTypes=[];
      self.systemInstanceType={id:null, name:'', description:'', parentContainer:''};
	    self.systemInstanceTypes=[];
      self.currentContainer = {id:2, parentContainer:{id:1}};
	    self.containersHierarchyForUser=[];
      self.systemInstanceHierarchy=[];


	    self.fetchAllContainerTypes = function(){
	    	$http.get( REST_SERVICE_URI +'coreController/availableContainerTypes/').then(
						       function(d) {
							        self.containerTypes = d.data;
						       },
	      					function(errResponse){
	      						console.error('error while fetching all container types from controller');
	      					}
				       );
	    };

      self.fetchAllSystemInstanceTypes = function(){
	    	$http.get( REST_SERVICE_URI +'coreController/availableSystemInstanceTypes/').then(
						       function(d) {
							        self.containerTypes = d.data;
						       },
	      					function(errResponse){
	      						console.error('error while fetching all container types from controller');
	      					}
				       );
	    };


      self.initContainerTypeForm = function(){
        if ($stateParams.updateContainerType != undefined) {
          self.systemInstanceType = $stateParams.updateContainerType;
      }

        // $http.get( REST_SERVICE_URI +'coreController/containersHierarchyToBeSelectedFromUser/'+self.currentContainer.parentContainer.id).then(
        //            function(d) {
        //              self.containersHierarchyForUser = d.data;
        //            },
        //           function(errResponse){
        //             console.error('Error while getting containers type from controller');
        //           }
        //        );
      };

      self.initSystemInstanceTypeForm = function(){
        if ($stateParams.updateSystemInstanceType != undefined) {
          self.systemInstanceType = $stateParams.updateSystemInstanceType;
          }
          $http.get( REST_SERVICE_URI +'coreController/allChildsSystemInstances/'+self.currentContainer.parentContainer.id).then(
                     function(d) {
                       self.systemInstanceHierarchy = d.data;
                     },
                    function(errResponse){
                      console.error('Error while getting containers type from controller');
                    }
                 );
      };

      self.submitCreateSystemInstanceForm = function() {
			  self.createContainerType(self.systemInstanceType);
	      };


	    self.createContainerType = function(container){
		   	$http.post( REST_SERVICE_URI +'coreController/newContainerType/', container).then(
			              function(response){
			            	  self.reset();
							  },
			              function(errResponse){
				               console.error('Error while creating Container Type.');
				              }
		           );
		 };


	    self.submitCreateContainerForm = function() {
			  self.createContainerType(self.containerType);
	      };

      self.submitSystemInstanceForm = function() {
  		  self.createContainerType(self.systemInstanceType);
  	   };

	    self.createContainerType = function(container){
		   	$http.post( REST_SERVICE_URI +'coreController/newContainerType/', container).then(
			              function(response){
			            	  self.reset();
							  },
			              function(errResponse){
				               console.error('Error while creating Container Type.');
				              }
		           );
		 };

		 self.reset = function(){
			   self.containerType={id:null, name:'', parentContainerType:null, description:''};
		     self.systemInstanceType={id:null, name:'', description:'', parentContainer:''};
		    };


  }

})();
