


(function () {
  'use strict';

  angular.module('BlurAdmin.pages.users', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('dashboard.users', {
          url: '/users',
          template : '<ui-view  autoscroll="true" autoscroll-body-top></ui-view>',
          abstract: true,
          controller: 'UsersPageCtrl',
          title: 'User Mngmt',
          sidebarMeta: {
            icon: 'ion-grid',
            order: 300,
          },
        }).state('dashboard.users.newUser', {
          url: '/newUser',
          templateUrl: 'app/pages/users/new/newUser.html',
          title: 'Create User',
          controller: 'UsersPageCtrl',
          sidebarMeta: {
            order: 0,
          },
        }).state('dashboard.users.listUsers', {
            url: '/listUsers',
            templateUrl: 'app/pages/users/list/listUsers.html',
            title: 'List Users',
            controller: 'UsersPageCtrl',
            sidebarMeta: {
              order: 0,
            },
          }).state('dashboard.users.updateUser', {
              url: '/updateUser',
              templateUrl: 'app/pages/users/new/newUser.html',
              title: 'School Update User',
              controller: "UsersPageCtrl",
              params: {
                  updateUser: "updateUser"
              }
          });
    $urlRouterProvider.when('/users','/users/newUser','/users/listUsers');
  }

})();
