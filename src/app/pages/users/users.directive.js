/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';
//Check Unique Value
  angular.module('BlurAdmin.pages.users')
  .directive("uniqueValue", ['$http',function($http) {
	  return {
	    restrict: 'A',
	    require: 'ngModel',
	    link: function (scope, element, attrs, ngModel) {
	      element.bind('blur', function (e) {
	        if (!ngModel || !element.val()) return;
	        var keyProperty = scope.$eval(attrs.uniqueValue);
	        var currentValue = element.val();
	        var data = [keyProperty.table, keyProperty.column, keyProperty.id, currentValue];
	        $http.post( 'http://localhost:8081/BackEnd/api/coreController/checkUniqueValue/', data)
	          .then(function (unique) {
	            //Ensure value that being checked hasn't changed
	            //since the Ajax call was made
	            if (currentValue == element.val()) {
	            	if (unique.data == true) {
	            		console.log('unique = '+unique.data);
	  	                ngModel.$setValidity('unique',unique.data);
					}else{
						console.log('unique = '+unique.messageText);
	  	                ngModel.$setValidity('unique',false);
					}
	              scope.$broadcast('show-errors-check-validity');
	            }
	          },
              function(errResponse){
	        	  for (var i = 0; i < errResponse.data.length; i++) {
	        		  console.log('Error = '+errResponse.data[i].exceptionKey);
				}
//	        	  console.log('unique = '+unique.messageText);
	              ngModel.$setValidity('unique',false);
	              scope.$broadcast('show-errors-check-validity');
	          }	);
	      });
	    }
	  }
	}]);

angular.module('BlurAdmin.pages.users').directive('compareTo', compareTo);
compareTo.$inject = [];

    function compareTo() {

        return {
            require: "ngModel",
            scope: {
                compareTolValue: "=compareTo"
            },
            link: function(scope, element, attributes, ngModel) {

                ngModel.$validators.compareTo = function(modelValue) {

                    return modelValue == scope.compareTolValue;
                };

                scope.$watch("compareTolValue", function() {
                    ngModel.$validate();
                });
            }
        };
    };

  angular.module('BlurAdmin.pages.users').directive('pwCheck', [function () {
	    return {
	        require: 'ngModel',
	        link: function (scope, elem, attrs, ctrl) {
	            var firstPassword = '#' + attrs.pwCheck;
	            elem.add(firstPassword).on('keyup', function () {
	                scope.$apply(function () {
	                    // console.info(elem.val() === $(firstPassword).val());
	                    ctrl.$setValidity('pwmatch', elem.val() === $(firstPassword).val());
	                });
	            });
	        }
	    }
	}]);



})();
