


(function () {
  'use strict';

  angular.module('BlurAdmin.pages.users')
      .controller('UsersPageCtrl', UsersPageCtrl);

  /** @ngInject */
  function UsersPageCtrl($scope, $filter, $http, $stateParams, editableOptions, editableThemes) {

	    var REST_SERVICE_URI = 'http://localhost:8081/BackEnd/api/';


	    var self = this;
	    self.user = {id:null, userName:'', address:'', name:'', email:'',
      gender:'0', suspended:'', birthDate:'', password:'', confirmPassword:''};
	    self.container = {id:null , name:'' , suspended:'' , address:''};
	    self.role = {id:null , name:'' , description:''};
	    self.users = [];
	    self.containers=[{id:'',name:'',containerType:'',role:[],showassign:false}];
      self.currentContainer = {id:2, parentContainer:{id:1}};
	    self.Roles=[];
	    self.UserContainerList = [{user:'' , container:'' , userContainerRole:[]}];
	    self.openEditUserForm = false;
	    self.containersWithThierRoles = [];

	    // This array will contain containerId and the roleId (assigned to user)
	    self.assignedroles=[];

	    // This array will contain list of roles in container assigned to user before submit
	    self.listofassignedroles=[];

	    self.fetchAllUsers = function(){
	    	$http.get( REST_SERVICE_URI + 'coreController/allUsers/').then(function(d) {
							        self.users = d.data;
	            					},
	      			function(errResponse){
	      						console.error('Error while fetching all users from core controller');
	            					});
	        };

	    self.submitUserForm = function() {
	    	var selectedNodes = [];
	  		self.user.containerRoles = [];
	  		selectedNodes = $(allContainersAndThierRoles).jstree().get_selected(true);
	  		for (var i = 0; i < selectedNodes.length; i++) {
          if (selectedNodes[i].original.entityTypeName == "role") {
            self.user.containerRoles.push({container:{id:selectedNodes[i].original.entityParentId}, role:{id:selectedNodes[i].original.entityId}});
          }
        }
	  		self.createUser(self.user);
	    };

	    self.createUser = function(user){
	  		$http.post( REST_SERVICE_URI + 'coreController/createUser/', user).then(
	      		  		  function(response){
			            		  $('#allContainersAndThierRoles').jstree("deselect_all");
			            		  $('#allContainersAndThierRoles').jstree("close_all");
			            		  self.reset();
			                       },
				              function(errResponse){
			                    	  console.error('Error while creating User (from controller).');
				              } );
	    };


	   self.deleteUser = function(id){
		   $http.delete( REST_SERVICE_URI + 'coreController/deleteUser/'+id).then(
	      		  		  self.fetchAllUsers,
				              function(errResponse){
					               console.error('Error while deleting User in controller.');
				              });
	    };


	    self.remove = function(id){
	  	  var box = $("#mb-remove-row");
	        box.addClass("open");
	        box.find(".mb-control-yes").one("click",function(){
	            box.removeClass("open");
	            self.deleteUser(id);
	        });
	        box.find(".mb-control-close").on("click",function(){
	            box.removeClass("open");
	        });
	    };


	    self.reset = function(){
	  	  self.user={id:null, userName:'', address:'', name:'', email:'',
        gender:'0', suspended:'', birthDate:'', password:'', confirmPassword:''};
	      $scope.userForm.$setPristine(); //reset Form
	    };

	    self.fetchAllContainers = function(){
	    	$http.get( REST_SERVICE_URI + 'coreController/allcontainers/').then(
	  			  function(result) {
						        self.containers = result.data;
						        	for(var i = 0; i < self.containers.length; i++){
	    						    	self.containers[i].role=[];
								}

						    for(var i = 0; i < self.containers.length; i++){
						    	for(var x = 0; x < self.listofassignedroles.length; x++){
						    		if(self.containers[i].name == self.listofassignedroles[x].containername){
						    			for(var y = 0; y < self.listofassignedroles[x].role.length; y++){
						    				self.containers[i].role.push({id:self.listofassignedroles[x].role[y].id});
						    			}
						    		}
						    	}
				             }
					       },
	  					function(errResponse){
	  						console.error('Error yyyyy');
	  					}
			       );
	    };

	    self.listContainersToUser = function(){
	  	  self.fetchAllContainers();
	  	  self.fetchAllRoles();
	  	  var box = $("#assign-user-to-containers");
	        box.addClass("open");
	        box.find(".mb-control-yes").on("click",function(){
	        box.removeClass("open");
	        });
	        box.find(".mb-control-close").on("click",function(){
	      	  box.removeClass("open");
	        });
	    };




	    self.fetchAllRoles = function(){
	    	$http.get( REST_SERVICE_URI + 'coreController/getRoles/').then(
					       function(result) {
					    	 self.Roles = result.data;
					       },
	  					function(errResponse){
	  						console.error('Error in get containers roles');
	  					}
			       );

	    };

	    self.dropdownMltiselectSettings = {
			    smartButtonMaxItems: 3,
			    enableSearch: true,
			    smartButtonTextConverter: function(itemText, originalItem) {
			        if (itemText === 'Jhon') {
			        return 'Jhonny!';
			        }
			        return itemText;
			    }
			};






	    self.currentroles = function(container){
	  	  if(self.assignedroles.length == 0){
	  		  alert("No Roles.....!");
	  	  }
	    };

	    self.showRolesAndDescription = function(){
	  	  $(".faq .faq-item .faq-title").click(function(){
	  	        var item = $(this).parent('.faq-item');

	  	        if(item.hasClass("active"))
	  	            $(this).find(".fa").removeClass("fa-angle-up").addClass("fa-angle-down");
	  	        else
	  	            $(this).find(".fa").removeClass("fa-angle-down").addClass("fa-angle-up");

	  	        item.toggleClass("active");

	  	        onresize(300);
	  	    });
	    };

	    self.initiateUserForm = function(){
	    	var loggedInUserId = 1;
	    	if ($stateParams.updateUser != undefined) {
	    		self.user = $stateParams.updateUser;
          self.user.confirmPassword = self.user.password;
          if (self.user.gender == 'MALE') {
            self.user.gender = '0';
            }else {
              self.user.gender = '1';
            }
            $http.get( REST_SERVICE_URI + 'coreController/getContainersHierarchyWithThierRoles/'+self.currentContainer.id).then(
               function(xas) {
                 self.containersWithThierRoles = xas.data;

                 $http.get( REST_SERVICE_URI + 'coreController/getContainerRolesForUser/'+ self.user.id).then(
         					       function(result) {
         					    	 self.user.containerRoles = result.data;
                         for (var i = 0; i < self.containersWithThierRoles.length; i++) {
                           for (var s = 0; s < self.containersWithThierRoles[i].children.length; s++) {
                             for (var x = 0; x < self.user.containerRoles.length; x++) {
                               if(self.containersWithThierRoles[i].children[s].entityId == self.user.containerRoles[x].role.id
                               && self.containersWithThierRoles[i].children[s].entityParentId == self.user.containerRoles[x].container.id){
                                 self.containersWithThierRoles[i].children[s].state = {selected:true};
                               }
                             }
                           }
                         }
                         $('#allContainersAndThierRoles').jstree({
                               'plugins': ["wholerow", "checkbox", "types"],
                               'core': {
                                   "themes" : {
                                       "responsive": false
                                   },
                                   'data': self.containersWithThierRoles
                               },
                               "types" : {
                                   "default" : {
                                       "icon" : "fa fa-folder icon-state-warning icon-lg"
                                   },
                                   "file" : {
                                       "icon" : "fa fa-file icon-state-warning icon-lg"
                                   }
                               }
                           });
         					       },
         	  					function(errResponse){
         	  						console.error('Error in get containers roles');
         	  					}
         			       );
               },
          function(errResponse){
            console.error('Error in get containers With Thier roles 11111');
          }
           );
			};

			if ($stateParams.updateUser == undefined) {
	    	$http.get( REST_SERVICE_URI + 'coreController/getContainersHierarchyWithThierRoles/'+self.currentContainer.id).then(
				       function(xas) {
				    	   self.containersWithThierRoles = xas.data;
				    	   $('#allContainersAndThierRoles').jstree({
				               'plugins': ["wholerow", "checkbox", "types"],
				               'core': {
				                   "themes" : {
				                       "responsive": false
				                   },
				                   'data': self.containersWithThierRoles
				               },
				               "types" : {
				                   "default" : {
				                       "icon" : "fa fa-folder icon-state-warning icon-lg"
				                   },
				                   "file" : {
				                       "icon" : "fa fa-file icon-state-warning icon-lg"
				                   }
				               }
				           });
				       },
					function(errResponse){
						console.error('Error in get containers With Thier roles 222');
					}
		       );
			};
	      };

}

})();
