


(function () {
  'use strict';

  angular.module('BlurAdmin.pages.schedualStructure', ['dx'])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('dashboard.schedualStructure', {
          url: '/schedualStructure',
          template : '<ui-view  autoscroll="true" autoscroll-body-top></ui-view>',
          abstract: true,
          controller: 'SchedualStructurePageCtrl',
          title: 'Schedual Structure Mngmt',
          sidebarMeta: {
            icon: 'ion-grid',
            order: 300,
          },
        }).state('dashboard.schedualStructure.newSchedualStructure', {
          url: '/newSchedualStructure',
          templateUrl: 'app/pages/schedualStructure/new/newSchedualStructure.html',
          title: 'Create Schedual Structure',
          controller: 'SchedualStructurePageCtrl',
          sidebarMeta: {
            order: 0,
          },
        }).state('dashboard.schedualStructure.listSchedualStructure', {
            url: '/listSchedualStructure',
            templateUrl: 'app/pages/schedualStructure/list/listSchedualStructure.html',
            title: 'List Schedual Structures',
            controller: 'SchedualStructurePageCtrl',
            sidebarMeta: {
              order: 0,
            },
          }).state('dashboard.schedualStructure.updateSchedualStructure', {
              url: '/updateSchedualStructure',
              templateUrl: 'app/pages/schedualStructure/new/newSchedualStructure.html',
              title: 'Update Schedual Structure',
              controller: "SchedualStructurePageCtrl",
              params: {
                  updateSchedualStructureID: "updateSchedualStructureID"
              }
          }).state('dashboard.schedualStructure.updateSchedualStructurePeriods', {
              url: '/updateSchedualStructurePeriods',
              templateUrl: 'app/pages/schedualStructure/new/newSchedualStructurePeriods.html',
              title: 'Update Schedual Structure Periods',
              controller: "SchedualStructurePageCtrl",
              params: {
                  updateSchedualStructurePeriodsBySSID: "updateSchedualStructurePeriodsBySSID"
              }
          }).state('dashboard.schedualStructure.mySchedual', {
              url: '/mySchedual',
              templateUrl: 'app/pages/schedualStructure/widgets/mySchedual.html',
              title: 'My Schedual',
              controller: "SchedualStructurePageCtrl",
              sidebarMeta: {
              order: 0,
            },
          }).state('dashboard.schedualStructure.schedual', {
              url: '/setSchedualURL',
              templateUrl: 'app/pages/schedualStructure/new/newClassRooms.html',
              title: 'Set Schedual',
              controller: "ClassRoomPageCtrl",
              data : {setSchedual : true},
              sidebarMeta: {
              order: 0,
            },
          });
    $urlRouterProvider.when('/schedualStructure','/schedualStructure/newSchedualStructure',
    '/schedualStructure/listSchedualStructure','/schedualStructure/updateSchedualStructure','/schedualStructure/updateSchedualStructurePeriods'
    ,'/schedualStructure/schedual');
  }

})();
