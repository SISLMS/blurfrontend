


(function () {
  'use strict';

  angular.module('BlurAdmin.pages.schedualStructure')
      .controller('SchedualStructurePageCtrl', SchedualStructurePageCtrl)
      .directive('afterRender', ['$timeout', function ($timeout) {
	    var def = {
	        restrict: 'A',
	        terminal: true,
	        transclude: false,
	        link: function (scope, element, attrs) {
	        	$timeout(scope.$eval(attrs.afterRender), 0);  //Calling a scoped method

	        }
	    };
	    return def;
	}]);

  function SchedualStructurePageCtrl($scope, $http, $stateParams, $state, $uibModal) {

	    var REST_SERVICE_URI = 'http://localhost:8081/BackEnd/api/';

      var self = this;
      self.schedualStructureName = '';
      self.schedualStructure = {id:null, name:''};
      self.schedualStructurePeriod = {day:null, schedualStructure:{}, schedualPeriodType:{id:null, text:null, color:null}, name:'', startDate:null, endDate:null};
      self.schedualStructurePeriodArray=[];
      self.days = ['SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY'];
      self.schedualPeriodType = {text:null, id:null, color:null};
      self.schedualPeriodTypeArrayOnLoad=[];
      self.schedualStructuresArray = [];
      $scope.initialData=[];
      self.currentUser = {id:1};


      self.getSchedualStructureTypes = function(){
        $http.get( REST_SERVICE_URI +'schedualStructureController/SchedualPeriodTypes/').then(
                    function(response){
                      for (var i = 0; i < response.data.length; i++) {
                        // self.schedualPeriodType = {text:null, id:null, color:null};
                        self.schedualPeriodType = {text:response.data[i].name, id:response.data[i].id, color:response.data[i].color};
                        self.schedualPeriodTypeArrayOnLoad.push(self.schedualPeriodType);
                      }
                },
                    function(errResponse){
                       console.error('Error while getting Schedual Period Types .');
                      }
               );
      };

      self.getSchedualStructureTypes();


      $scope.getSchedualStructurePeriods = function(schedualStructureID){
           $http.get( REST_SERVICE_URI +'schedualStructureController/getSchedualStructurePeriodsBySSID/'+schedualStructureID).then(
                       function(res){
                          $scope.initialData = [];
                          var scheduler = $("#schedulerContainer").dxScheduler("instance");
                         for (var i = 0; i < res.data.length; i++) {
                          // scheduler.addAppointment({id:res.data[i].id, text: res.data[i].name, schedualStructure:{id:schedualStructure.id, name:schedualStructure.name}, ownerId: res.data[i].schedualPeriodType.id,
                          //   startDate: new Date(res.data[i].startDate), endDate: new Date(res.data[i].endDate)});
                           $scope.initialData.push({id:res.data[i].id, text: res.data[i].name, schedualStructure:{id:schedualStructure.id, name:schedualStructure.name}, ownerId: res.data[i].schedualPeriodType.id,
                             startDate: new Date(res.data[i].startDate), endDate: new Date(res.data[i].endDate)});
                         }

                        //use addAppointment() to style view
                        $("#schedulerContainer").dxScheduler({
                          dataSource: $scope.initialData
                        });
                        // scheduler.addAppointment({text: "sssssssss",  ownerId: 2, startDate: new Date(2015, 4, 26, 9, 30), endDate: new Date(2015, 4, 26, 11, 30)});
                        // scheduler.deleteAppointment({text: "sssssssss",  ownerId: 2, startDate: new Date(2015, 4, 26, 9, 30), endDate: new Date(2015, 4, 26, 11, 30)});
                        if(res.data[0] != undefined){
                        self.schedualStructure = res.data[0].schedualStructure;
                        }
                   },
                       function(errResponse){
                          console.error('Error while getting All Schedual Structure Periods.');
                         }
                  );
      };


      self.getSchedualStructure = function(schedualStructureID){
           $http.get( REST_SERVICE_URI +'schedualStructureController/getSchedualStructureBySSID/'+schedualStructureID).then(
                       function(res){
                         self.schedualStructure = res.data;
                       },
                       function(errResponse){
                          console.error('Error while getting All Schedual Structure.');
                         }
                  );
      };

      self.initMySchedual = function(){
        $http.get( REST_SERVICE_URI +'schedualStructureController/getSchedualStructurePeriodsToUser/'+self.currentUser.id).then(
                    function(res){
                       $scope.initialData = [];
                       var scheduler = $("#mySchedual").dxScheduler("instance");
                      for (var i = 0; i < res.data.length; i++) {
                       // scheduler.addAppointment({id:res.data[i].id, text: res.data[i].name, schedualStructure:{id:schedualStructure.id, name:schedualStructure.name}, ownerId: res.data[i].schedualPeriodType.id,
                       //   startDate: new Date(res.data[i].startDate), endDate: new Date(res.data[i].endDate)});
                        $scope.initialData.push({id:res.data[i].id, text: res.data[i].name, ownerId: res.data[i].schedualPeriodType.id,
                          startDate: new Date(res.data[i].startDate), endDate: new Date(res.data[i].endDate)});
                      }

                     //use addAppointment() to style view
                     $("#mySchedual").dxScheduler({
                       dataSource: $scope.initialData
                     });
                     // scheduler.addAppointment({text: "sssssssss",  ownerId: 2, startDate: new Date(2015, 4, 26, 9, 30), endDate: new Date(2015, 4, 26, 11, 30)});
                     // scheduler.deleteAppointment({text: "sssssssss",  ownerId: 2, startDate: new Date(2015, 4, 26, 9, 30), endDate: new Date(2015, 4, 26, 11, 30)});
                     self.schedualStructure = res.data[0].schedualStructure;
                },
                    function(errResponse){
                       console.error('Error while getting All Schedual Structure Period To User .');
                      }
               );

      };


      self.fetchAllSchedualStructures = function(){
        $http.get( REST_SERVICE_URI +'schedualStructureController/getAllSchedualStructures/').then(
                    function(response){
                      self.schedualStructuresArray = response.data;
                },
                    function(errResponse){
                       console.error('Error while getting All Schedual Structure .');
                      }
               );
      };

      self.assignMySchedualStartTime = function(){
        $http.get( REST_SERVICE_URI +'schedualStructureController/getTermStartAndEnd/').then(
                    function(response){
                      // console.log(new Date(2019, 10, 4));
                      console.log(new Date(response.data[0]));
                      $("#schedulerContainer").dxScheduler({
                        currentDate: new Date(response.data[0])
                      });
                      $scope.missionCompled();
                },
                    function(errResponse){
                       console.error('Error while getting All Schedual Structure .');
                      }
               );
      };

      self.initNewSchedualStructureForm = function(){
    	  if ($stateParams.updateSchedualStructureID != undefined) {
    	  self.getSchedualStructure($stateParams.updateSchedualStructureID);
        }
      };

      self.initNewSchedualStructurePeriodsForm = function(){
        self.assignMySchedualStartTime();

        // $("#schedulerContainer").dxScheduler({
        //     currentDate: new Date(2015, 4, 25)
        // });
    	  if ($stateParams.updateSchedualStructureID != undefined) {
    	  $scope.getSchedualStructurePeriods($stateParams.updateSchedualStructureID);
        }else if ($stateParams.updateSchedualStructurePeriodsBySSID != undefined) {
          self.schedualStructure = $stateParams.updateSchedualStructurePeriodsBySSID;
    	  $scope.getSchedualStructurePeriods($stateParams.updateSchedualStructurePeriodsBySSID.id);
        }else if ($state.current.data != undefined && $state.current.data.setSchedual != undefined) {
          $scope.getSchedualStructurePeriods(31);
        };
      };

      self.createSchedualStructure = function() {
          $http.post( REST_SERVICE_URI +'schedualStructureController/newSchedualStructure/',
          self.schedualStructure).then(
  			              function(response){
                        self.schedualStructure = {id:null, name:''};
  							  },
  			              function(errResponse){
  				               console.error('Error while creating Schedual Structure.');
  				              }
  		           );
      };

      // self.submit = function() {
      //   self.createSchedualStructureObjectFromVarData();
	    // 	self.createSchedualStructurePeriods();
	    // };
      //
      // self.createSchedualStructurePeriods = function() {
      //   $http.post( REST_SERVICE_URI +'schedualStructureController/newSchedualStructurePeriods/',
      //   self.schedualStructurePeriodArray).then(
			//               function(response){
      //                 $("#schedulerContainer").dxScheduler({
      //                   dataSource: []
      //                 });
      //                 self.schedualStructure = {id:null, name:''};
      //                 $scope.initialData=[];
			// 				  },
			//               function(errResponse){
			// 	               console.error('Error while creating Schedual Structure.');
			// 	              }
		  //          );
	    //   };
      //
      //
      //
      //   self.createSchedualStructureObjectFromVarData =  function(){
      //     self.schedualStructurePeriodArray=[];
      //     for (var i = 0; i < $scope.initialData.length; i++) {
      //
      //       for (var x = 0; x < self.schedualPeriodTypeArrayOnLoad.length; x++) {
      //         if(self.schedualPeriodTypeArrayOnLoad[x].id === $scope.initialData[i].ownerId){
      //           self.schedualStructurePeriod.schedualPeriodType = self.schedualPeriodTypeArrayOnLoad[x];
      //         }
      //       }
      //       // self.schedualStructure.name = self.schedualStructureName;
      //       self.schedualStructurePeriod.id = $scope.initialData[i].id;
      //       self.schedualStructurePeriod.name = $scope.initialData[i].text;
      //       self.schedualStructurePeriod.day = self.days[$scope.initialData[i].startDate.getDay()];
      //       self.schedualStructurePeriod.schedualStructure = self.schedualStructure;
      //       self.schedualStructurePeriod.startDate = $scope.initialData[i].startDate;
      //       self.schedualStructurePeriod.endDate = $scope.initialData[i].endDate;
      //       self.schedualStructurePeriodArray.push(self.schedualStructurePeriod);
      //       self.schedualStructurePeriod = {day:null, schedualStructure:{}, schedualPeriodType:{id:null}, text:'', startDate:null, endDate:null};
      //     }
      //   };


        $scope.initializedHandler = function (e) {
            if ($state.current.data != undefined && $state.current.data.setSchedual != undefined) {
              $scope.editing = {
               allowAdding: false,
               allowUpdating: false,
               allowDeleting: false,
               allowResizing: false,
               allowDragging: false
             };
             $("#schedulerContainer").dxScheduler({
               onAppointmentClick : $scope.schedulerContainerOnAppointmentClick,
               onAppointmentDblClick : $scope.schedulerContainerOnAppointmentClick
             });


            };

        };

        $scope.addingHandler = function (e) {
            for (var i = 0; i < $scope.initialData.length; i++) {
              if ($scope.initialData[i].startDate <= e.appointmentData.endDate && e.appointmentData.endDate <= $scope.initialData[i].endDate) {
                DevExpress.ui.dialog.alert($scope.initialData[i].text + "تاريخ التهايه متقاطع مع الفتره ", "Intersectionsssss");
                e.cancel.resolve(true);
              }
              if ($scope.initialData[i].startDate <= e.appointmentData.startDate && e.appointmentData.startDate <= $scope.initialData[i].endDate) {
                DevExpress.ui.dialog.alert($scope.initialData[i].text + "تاريخ البداية متقاطع مع الفتره ", "Intersectionsssss");
                e.cancel.resolve(true);
              }
            }
            // e.cancel = $.Deferred();
            // DevExpress.ui.dialog.confirm("هل انت متاكد?", "Confirm").done(function (dialogResponse) {
            //     e.cancel.resolve(!dialogResponse);
            // });
            //  DevExpress.ui.notify("add" +" \"" + e.appointmentData.text + "\"" + " task", "succes", 800);

            self.schedualStructurePeriod = {day:null, schedualStructure:{}, schedualPeriodType:{id:null}, text:'', startDate:null, endDate:null};
            for (var x = 0; x < self.schedualPeriodTypeArrayOnLoad.length; x++) {
              if(self.schedualPeriodTypeArrayOnLoad[x].id === e.appointmentData.ownerId){
                self.schedualStructurePeriod.schedualPeriodType = self.schedualPeriodTypeArrayOnLoad[x];
              }
            }
            self.schedualStructurePeriod.name = e.appointmentData.text;
            self.schedualStructurePeriod.day = self.days[e.appointmentData.startDate.getDay()];
            self.schedualStructurePeriod.schedualStructure = self.schedualStructure;
            self.schedualStructurePeriod.startDate = e.appointmentData.startDate;
            self.schedualStructurePeriod.endDate = e.appointmentData.endDate;

            $http.post( REST_SERVICE_URI +'schedualStructureController/newSchedualStructurePeriod/', self.schedualStructurePeriod).then(
    			              function(response){
                          if ($stateParams.updateSchedualStructurePeriodsBySSID != undefined) {
                              self.schedualStructure = $stateParams.updateSchedualStructurePeriodsBySSID;
                      	      $scope.getSchedualStructurePeriods($stateParams.updateSchedualStructurePeriodsBySSID.id);
                            }
    							  },
    			              function(errResponse){
    				               console.error('Error while creating Schedual Structure Period.');
    				              }
    		           );
        };

        $scope.updatingHandler = function (e) {
          for (var i = 0; i < $scope.initialData.length; i++) {
            if ($scope.initialData[i].id !== e.newData.id) {
            if ($scope.initialData[i].startDate <= e.newData.endDate && e.newData.endDate <= $scope.initialData[i].endDate) {
              DevExpress.ui.dialog.alert($scope.initialData[i].text + "تاريخ التهايه متقاطع مع الفتره ", "Intersectionsssss");
              e.cancel.resolve(true);
            }
            if ($scope.initialData[i].startDate <= e.newData.startDate && e.newData.startDate <= $scope.initialData[i].endDate) {
              DevExpress.ui.dialog.alert($scope.initialData[i].text + "تاريخ البداية متقاطع مع الفتره ", "Intersectionsssss");
              e.cancel.resolve(true);
            }
          }
        }
            // e.cancel = $.Deferred();
            // DevExpress.ui.dialog.confirm("Update appointment?", "Confirm").done(function (dialogResponse) {
            //     e.cancel.resolve(!dialogResponse);
            // });

            self.schedualStructurePeriod = {day:null, schedualStructure:{}, schedualPeriodType:{id:null}, text:'', startDate:null, endDate:null};
            for (var x = 0; x < self.schedualPeriodTypeArrayOnLoad.length; x++) {
              if(self.schedualPeriodTypeArrayOnLoad[x].id === e.newData.ownerId){
                self.schedualStructurePeriod.schedualPeriodType = self.schedualPeriodTypeArrayOnLoad[x];
              }
            }
            self.schedualStructurePeriod.id = e.newData.id;
            self.schedualStructurePeriod.name = e.newData.text;
            self.schedualStructurePeriod.day = self.days[e.newData.startDate.getDay()];
            self.schedualStructurePeriod.schedualStructure = self.schedualStructure;
            self.schedualStructurePeriod.startDate = e.newData.startDate;
            self.schedualStructurePeriod.endDate = e.newData.endDate;
            $http.post( REST_SERVICE_URI +'schedualStructureController/updateSchedualStructurePeriod/', self.schedualStructurePeriod).then(
    			              function(response){},
    			              function(errResponse){
    				               console.error('Error while update Schedual Structure period.');
    				              }
    		           );

        };
        $scope.deletingHandler = function (e) {
            e.cancel = $.Deferred();
            DevExpress.ui.dialog.confirm("Delete appointment?", "Confirm").done(function (dialogResponse) {
                e.cancel.resolve(!dialogResponse);
                if(dialogResponse){
                  self.schedualStructurePeriod = {day:null, schedualStructure:{}, schedualPeriodType:{id:null}, text:'', startDate:null, endDate:null};
                  for (var x = 0; x < self.schedualPeriodTypeArrayOnLoad.length; x++) {
                    if(self.schedualPeriodTypeArrayOnLoad[x].id === e.appointmentData.ownerId){
                      self.schedualStructurePeriod.schedualPeriodType = self.schedualPeriodTypeArrayOnLoad[x];
                    }
                  }
                  self.schedualStructurePeriod.id = e.appointmentData.id;
                  self.schedualStructurePeriod.name = e.appointmentData.text;
                  self.schedualStructurePeriod.day = self.days[e.appointmentData.startDate.getDay()];
                  self.schedualStructurePeriod.schedualStructure = self.schedualStructure;
                  self.schedualStructurePeriod.startDate = e.appointmentData.startDate;
                  self.schedualStructurePeriod.endDate = e.appointmentData.endDate;

                  $http.post( REST_SERVICE_URI +'schedualStructureController/deleteSchedualStructurePeriod/', self.schedualStructurePeriod).then(
                              function(response){
                          },
                              function(errResponse){
                                 console.error('Error while creating Schedual Structure Period.');
                                }
                         );
                }
            });
        };




	    $scope.missionCompled = function()
	    {
	    	//alert(document.getElementsByClassName("dx-scheduler-header-panel-cell").length)
	    var slides = document.getElementsByClassName("dx-scheduler-header-panel-cell");
	    for(var i = 0; i < slides.length; i++)
	    {
	       slides.item(i).innerHTML= slides.item(i).innerHTML.replace(/\d+/g, '');;
	    }
	    var slides = document.getElementsByClassName("dx-scheduler-header");
	    if(slides.length>0){
	    	slides.item(0).style.display='none'
	    }
      var header = document.getElementsByClassName("dx-scheduler-all-day-panel");
      if(header.length>0){
	    	header.item(0).style.display='none'
	    }
      var headerTitle = document.getElementsByClassName("dx-scheduler-all-day-title");
      if(headerTitle.length>0){
	    	headerTitle.item(0).style.display='none'
	    }
      var options = document.getElementsByClassName("options");
      if(options.length>0){
	    	options.item(0).style.display='none'
	    }
//	    dx-scheduler-header dx-widget
	    };



      $scope.editing = {
       allowAdding: true,
       allowUpdating: true,
       allowDeleting: true,
       allowResizing: true,
       allowDragging: true
   };
   $scope.useDropDownViewSwitcher = false;
   $scope.disabledValue = false;

   var switchModeNames = ["Tabs", "Drop-Down Menu"];
   $scope.schedulerOptions = {
       dataSource: $scope.initialData,
       views: ["agenda", "month", "week", "workWeek", "day"],
       currentView: "week",
       useDropDownViewSwitcher: false,
       firstDayOfWeek: 6,
       startDayHour: 8,
       endDayHour: 19,
       rtlEnabled:true,
       onInitialized: $scope.initializedHandler,
       onAppointmentAdding: $scope.addingHandler,
       onAppointmentUpdating: $scope.updatingHandler,
       onAppointmentDeleting: $scope.deletingHandler,
       bindingOptions: {
           editing: "editing",
           useDropDownViewSwitcher: "useDropDownViewSwitcher"
       },
       resources: [{
           fieldExpr: "ownerId",
           label:"النوع",
           allowMultiple: false,
           dataSource: self.schedualPeriodTypeArrayOnLoad
       }],
       width: "100%",
       height: 600
   };

   $scope.schedulerOptionsMySchedual = {
       dataSource: $scope.initialData,
       views: ["agenda", "month", "week", "workWeek", "day"],
       currentView: "week",
       currentDate: new Date(2015, 4, 25),
       useDropDownViewSwitcher: false,
       firstDayOfWeek: 6,
       startDayHour: 8,
       endDayHour: 19,
       rtlEnabled:true,

       onAppointmentAdding: $scope.addingHandler,
       onAppointmentUpdating: function(e) {
          e.cancel = true;
        },
       onAppointmentDeleting: $scope.deletingHandler,
       onAppointmentClick : function(e) {
          e.cancel = true;
        },
       onAppointmentDblClick : function(e) {
          e.cancel = true;
        },
       onCellClick: function(e) {
          e.cancel = true;
        },
        editing:{
          allowAdding:false,
          allowDeleting:false,
          allowDragging:false,
          allowResizing:false,
          allowUpdating:false
        },
       bindingOptions: {
           useDropDownViewSwitcher: "useDropDownViewSwitcher"
       },
       resources: [{
           fieldExpr: "ownerId",
           label:"النوع",
           allowMultiple: false,
           dataSource: self.schedualPeriodTypeArrayOnLoad
       }],
       width: "100%",
       height: 600
   };

   $scope.useDropDownViewSwitcherOptions = {
       items: switchModeNames,
       width: 200,
       value: switchModeNames[0],
       onValueChanged: function(data) {
           $scope.useDropDownViewSwitcher = data.value === switchModeNames[1];
       }
   };
   $scope.allowAddingOptions = {
       text: "Allow adding",
       bindingOptions: {
           value: "editing.allowAdding"
       }
   };
   $scope.allowUpdatingOptions = {
       text: "Allow updating",
       onValueChanged: function(data) {
           $scope.disabledValue = !data.value;
       },
       bindingOptions: {
           value: "editing.allowUpdating"
       }
   };
   $scope.allowDeletingOptions = {
       text: "Allow deleting",
       bindingOptions: {
           value: "editing.allowDeleting"
       }
   };
   $scope.allowResizingOptions = {
       text: "Allow resizing",
       bindingOptions: {
           value: "editing.allowResizing",
           disabled: "disabledValue"
       }
   };
   $scope.allowDraggingOptions = {
       text: "Allow dragging",
       bindingOptions: {
           value: "editing.allowDragging",
           disabled: "disabledValue"
       }
   };

   self.schedualClassRoomOptions = {
     dataSource : self.classRooms,
     width: 'auto'

   };
}
})();
